<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%-- ValueStack里面的值：<s:property value="username"/><br/>
	ActionStack里面的值：<s:property value="#username"/><br/>
	StackContext里面的值：<s:property value="#attr.username"/><br/>
	StackContext里面的值：<s:property value="#request.username"/> --%>
	
	用户名&nbsp;：<s:property value="user.username"/><br/>
	所在城市：<s:property value="user.contry"/><br/>
	选择权限是：<s:property value="user.role"/>
</body>
</html>