<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		String title = "提交";
		request.setAttribute("title", title);
	%>

	<%-- <s:submit value="%{#attr.title}"/>
	<s:if test="'java' in {'java','c'}">
		在里面
		<s:else>
			不在里面
		</s:else>
	</s:if> --%>

	<s:form action="login">
		<s:select list="#{'1':'管理员','2':'普通员工','3':'店长' }" listKey="key"
			listValue="value" name="user.role" />
		<s:submit value="%{#attr.title }"/>
	</s:form>



</body>
</html>