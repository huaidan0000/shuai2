<%@page import="shuai2.entity.OgnlInfo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<% OgnlInfo ognlInfo=new OgnlInfo();
		request.setAttribute("ognlInfo", ognlInfo);
	%>
	<!-- 执行OGNL的费静态方法 -->
	<s:property value="#attr.ognlInfo.sayHello('kognshuai')"/>
	<s:property value="@shuai2.entity.OgnlInfo@sayBey('kongshuai')"/>
</body>
</html>