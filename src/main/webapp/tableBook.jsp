<%@page import="java.util.ArrayList"%>
<%@page import="shuai2.entity.Book"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		List<Book> books=new ArrayList<Book>();
		Book b1=new Book();
		b1.setIsbn("111");
		b1.setName("kong");
		b1.setPrice(11111);
		
		Book b2=new Book();
		b2.setIsbn("1111");
		b2.setName("konga");
		b2.setPrice(11111111);
		
		Book b3=new Book();
		b3.setIsbn("111qw");
		b3.setName("kongre");
		b3.setPrice(11111453);
		
		books.add(b1);
		books.add(b2);
		books.add(b3);
		request.setAttribute("books", books);
	%>
	
	<table>
		<s:iterator id="book" value="#attr.books" status="st">
			<s:if test="#st.odd">
				<tr bgcolor="yellow">
			</s:if>
			<s:else >
				<tr bgcolor="blue">
			</s:else>
			<td><s:property value="#book.name"/></td>
			<td><s:property value="#book.isbn"/></td>
			<td><s:property value="#book.price"/></td>
		</s:iterator>
	</table>
</body>
</html>