<%@page import="shuai2.entity.Book"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		Map<String,Object> maps=new HashMap<String,Object>();
		Book b1=new Book("11","gfdsa",122);
		Book b2=new Book("12","gfdsa1",1221);
		Book b3=new Book("13","gfdsa2",1222);
		//把b1，b2，b3的isbn作为键值，他们自己作为值
		maps.put(b1.getIsbn(), b1);
		maps.put(b2.getIsbn(), b2);
		maps.put(b3.getIsbn(), b3);
		request.setAttribute("maps", maps);
	%>
	<s:iterator id="book" value="#attr.maps">
		<!-- book.key是maps的键，book.value是maps的值即b1 b2 b3 -->
		<s:property value="#book.key"/>
		<s:property value="#book.value.name"/>
		<s:property value="#book.value.price"/>
		<br/>
	</s:iterator>
	
</body>
</html>