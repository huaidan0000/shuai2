<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- 实例化一个book javabean对象，并存储到stackContext中 -->
	<s:bean id="book" name="shuai2.entity.Book">
		<!-- 为book对象的isbn赋值 -->
		<s:param name="isbn" value="123455"/>
		<!-- 为book对象的name赋值 -->
		<s:param name="name" value="Html"/>
	</s:bean>
	book对象的属性值：<br/>
	isbn:<s:property value="#book.isbn"/>
	name:<s:property value="#book.name"/>
	调用book对象的getAllBooks方法获取全部书籍列表：<br/>
	<c:forEach var="book" items="${book.allBooks }">
		<li>${book }</li>	
	</c:forEach>
	<br/>
	
	
	<s:bean id="today" name="java.util.Date"/>
	<h3>
		今天是：<s:date name="#today" format="yyyy年MM月dd日"/>
	</h3>
</body>
</html>